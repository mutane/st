@extends('layouts.appcozinha')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }} 
                        </div>
                    @endif
                   
                </div>
            </div>
        </div>
    </div>
    <form>
    	@csrf
    </form>

     
   <cozinha
    :Getuser= "{{ Auth::user()->toJson() }}" >
    </cozinha>
</div>


@endsection
{{-- 	<script type="text/javascript">
		const app = new  Vue({
		    el: '#app',
		    data:{
		    	coments:{},
		    	commentBox:'dat',
		    	post:{!! $posts->toJson() !!},
		    	user:{!! Auth::check()? Auth::user()->toJson() :'null' !!}
		    },

		    mounted(){

		    },
		    methods:{
		    	getComments(){
		    		axios.get('/api/posts/${this.post.id}/comments').then((res) => {
		    			this.coments = res.data
		    		}).catch(function(error) {
		    			console.log(error);
		    		});
		    	},
		    	postComments(){
		    		axios.post('/api/posts/${this.post.id}/comments',{
		    			api_token: this.user.api_token,
		    			body: this.commentBox
		    		}).then((res) => {
		    			this.coments.unshift(res.data);
		    			this.commentBox = '';
		    		}).catch(function (error){
		    			console.log(error);
		    		});
		    	}
		    }

		});
	</script> --}}