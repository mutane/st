<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return redirect()->route('cliente');
});
//Auth::routes();
Auth::routes([
  'register' => true, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
]);


Route::get('/admin', 'HomeController@index')->name('admin')->middleware(['auth','admin']);
Route::get('/gestor',function(){ return url()->current(); })->name('gestor')->middleware(['auth','gestor']);
Route::get('/caixa',function(){ return url()->current(); })->name('caixa')->middleware(['auth','caixa']);

Route::get('/cozinha','CozinhaController@index')->name('cozinha')->middleware(['auth','cozinha']);

Route::get('/entregador',function(){ return url()->current(); })->name('entregador')->middleware(['auth','entregador']);
Route::get('/transportador',function(){ return url()->current(); })->name('transportador')->middleware(['auth','transportador']);
Route::get('/cliente',function(){ return url()->current(); })->name('cliente')->middleware(['auth','cliente']);

Route::get('/all/users','Auth\RegisterController@getUsers')->name('users_all');

Route::get('/{id}/user','Auth\RegisterController@getUser');
/**
<template>

    <div class="offset">
    <div class="col-12 col-xl-12 ">
        <div class="card shadow-sm">
            <div class="card-header">
                <button type="button" class="btn btn-pill btn-primary z-depth-2" data-toggle="modal" data-target="#newUser" @click="effect">Novo usuario</button>
            </div>
            <table class="table table-striped">
                <thead>
                                            <tr>
                                                <th style="width:40%;">Name</th>
                                                <th style="width:25%">Phone Number</th>
                                                <th class="d-none d-md-table-cell" style="width:25%">Date of Birth</th>
                                                <th colspan="2">Actions</th>
                                            </tr>
                </thead>
                <tbody >
                                            <tr v-for="user in displayedPosts">
                                                <td><img src="img/avatars/avatar-5.jpg" width="30" height="30" class="rounded-circle mr-2" alt="Avatar">{{ user.name }}</td>
                                                <td>{{user.role.nome}}</td>
                                                <td class="d-none d-md-table-cell">{{ user.created_at}}</td>
                                                <td class="table-action">
                                                    <a href="#">
                                                        <button class="btn btn-pill btn-warning z-depth-1" @click="update(user.id)"> Edit</button>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#"><button class="btn btn-pill btn-danger z-depth-1">Delete</button></a>
                                                </td>
                                            </tr>
                </tbody>
            </table>
             <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <button type="button" class="page-link" v-if="page != 1" @click="page--"> Previous </button>
                </li>
                <li class="page-item">
                    <button type="button" class="page-link" v-for="pageNumber in pages.slice(page-1, page+5)" @click="page = pageNumber"> {{pageNumber}} </button>
                </li>
                <li class="page-item">
                    <button type="button" @click="page++" v-if="page < pages.length" class="page-link"> Next </button>
                </li>
            </ul>
        </nav>
        </div>
    </div>
    <div class="modal fade " id="newUser" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-md animated zoomIn speeder" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Usuario</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="effect"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                                <div class="modal-body m-3">
                                                     <div :class="error.class" role="alert"><div class="alert-message"><strong>{{ error.alert }}!</strong>  {{ error.message }}</div></div>
                                                  <form>
                                                    <div class=" row col-sm-12">
                                                       <div class="input-group input-group-navbar col-sm-6"><input type="text" class="form-control" placeholder="Nome" v-model="name" autocomplete="name">
                                                       </div>

                                                        <div class="input-group input-group-navbar col-sm-6"><input type="email" class="form-control" placeholder="Email"  v-model="emails">
                                                        </div>
                                                    </div>
                                                    <div class="row col-sm-12">
                                                        <div class="input-group input-group-navbar col-sm-12">
                                                            <select class=" form-control custom-select" v-model="role">
                                                              
                                                              <option v-for = "role in permissao" :value="role.id">{{role.nome }}</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class=" row col-sm-12">
                                                       <div class="input-group input-group-navbar col-sm-6"><input type="password" class="form-control" placeholder="Passord" v-model="passwords"></div>

                                                        <div class="input-group input-group-navbar col-sm-6"><input type="password" class="form-control" placeholder="Confirmar password"  v-model="confirm_password">
                                                        </div>
                                                    </div>

                                                  </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-pill btn-secondary" data-dismiss="modal" @click="effect">Close</button>
                                                    <button type="button" class="btn btn-pill btn-secondary" @click="register">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
    </div>
</div>
</template>

<script type="text/javascript">

export default{



























     props: ['roles'],
    data () {
        return {
            page: 1,
            perPage: 2,
            usersget:[],
            pages: [],   
            loading:false,
            name:'',
            role:'',
            emails:'',
            passwords:'',
            confirm_password:'',
            url:'',
            req:[],
            error:{'class':"alert alert-warning alert-dismissible","alert":"Alerta","message":"Novo usuari"},
            permissao:this.roles
        }
    },
   
    methods:{
        setPages () {
            let numberOfPages = Math.ceil(this.usersget.length / this.perPage);
            for (let index = 1; index <= numberOfPages; index++) {
                console.log(this.usersget.length);
                this.pages.push(index);
            }
        },
        paginate (posts) {
            let page = this.page;
            let perPage = this.perPage;
            let from = (page * perPage) - perPage;
            let to = (page * perPage);
            return  posts.slice(from, to);
        },
        fechUsers(){
                    this.loading = true;
                    this.usersget = [];
                    this.url = "/all/users";
                    axios.get(this.url).then((response)=>{
                       console.log(response.data);
                       const data = response.data;
                       this.usersget = data;
                       this.loading = false; 
                    });
                },
                effect(){
                     this.error = {'class':"alert alert-warning alert-dismissible","alert":"Alerta","message":"Novo usuario"}
                },

                register(){

                    this.req = {'name':this.name,'email':this.emails,'password':this.passwords,'password_confirmation':this.confirm_password,'role_id':this.role},
                    axios.post('/register',
                    this.req,
                    this.role,
                    this.emails,
                    this.passwords
                    
                        ).then((response) =>{
                            console.log(response);
                            this.fechUsers();
                            $('#newUser').modal('hide');
                                this.name='';
                                this.role='';
                                this.emails='';
                                this.passwords='';
                                this.confirm_password='';

                            }).catch((err)=>{
                                this.error.message =(err.response.data.errors.name?", "+err.response.data.errors.name:'') + (err.response.data.errors.email?", "+err.response.data.errors.email:'')+(err.response.data.errors.password? ", "+err.response.data.errors.password:'')+(err.response.data.errors.role_id?err.response.data.errors.role_id:'');
                                this.error.class = "alert alert-danger alert-dismissible";
                                this.error.alert = "Erro";
                                console.log(err.response);
                            });
                },

                update(id){
                    console.log(id);
                }
    },
      computed: {
        displayedPosts () {
            return this.paginate(this.usersget);
        }
    },
   
    watch: {
        posts () {
            this.setPages();
        }
    },
    created(){
        this.fechUsers();
    },
    filters: {
        trimWords(value){
            return value.split(" ").splice(0,20).join(" ") + '...';
        }
    }
}

</script>



 <table class="table table-bordered">
    <thead>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>User ID</th>
      </tr>
    </thead>
    <tbody>
    <tr v-for="p in displayedPosts">
      <td>{{p.first}}</td>
      <td>{{p.last}}</td>
      <td>{{p.suffix}}</td>
    </tr>
    </tbody>
  </table>









  <template>

    <div class="offset">
    <div class="col-12 col-xl-12 ">
        <div class="card shadow-sm">
            <div class="card-header">
                <button type="button" class="btn btn-pill btn-primary z-depth-2" data-toggle="modal" data-target="#newUser" @click="effect">Novo usuario</button>
            </div>
            <table class="table table-striped">
                <thead>
                                            <tr>
                                                <th style="width:40%;">Name</th>
                                                <th style="width:25%">Phone Number</th>
                                                <th class="d-none d-md-table-cell" style="width:25%">Date of Birth</th>
                                                <th colspan="2">Actions</th>
                                            </tr>
                </thead>
                <tbody >
                                            <tr v-for="user in displayedPosts">
                                                <td><img src="img/avatars/avatar-5.jpg" width="30" height="30" class="rounded-circle mr-2" alt="Avatar">{{ user.name }}</td>
                                                <td>{{user.role.nome}}</td>
                                                <td class="d-none d-md-table-cell">{{ user.created_at}}</td>
                                                <td class="table-action">
                                                    <a href="#">
                                                        <button class="btn btn-pill btn-warning z-depth-1" @click="update(user.id)"> Edit</button>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#"><button class="btn btn-pill btn-danger z-depth-1">Delete</button></a>
                                                </td>
                                            </tr>
                </tbody>
            </table>
             <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <button type="button" class="page-link" v-if="page != 1" @click="page--"> Previous </button>
                </li>
                <li class="page-item">
                    <button type="button" class="page-link" v-for="pageNumber in pages.slice(page-1, page+5)" @click="page = pageNumber"> {{pageNumber}} </button>
                </li>
                <li class="page-item">
                    <button type="button" @click="page++" v-if="page < pages.length" class="page-link"> Next </button>
                </li>
            </ul>
        </nav>
        </div>
    </div>
    <div class="modal fade " id="newUser" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-md animated zoomIn speeder" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Usuario</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="effect"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                                <div class="modal-body m-3">
                                                     <div :class="error.class" role="alert"><div class="alert-message"><strong>{{ error.alert }}!</strong>  {{ error.message }}</div></div>
                                                  <form>
                                                    <div class=" row col-sm-12">
                                                       <div class="input-group input-group-navbar col-sm-6"><input type="text" class="form-control" placeholder="Nome" v-model="name" autocomplete="name">
                                                       </div>

                                                        <div class="input-group input-group-navbar col-sm-6"><input type="email" class="form-control" placeholder="Email"  v-model="emails">
                                                        </div>
                                                    </div>
                                                    <div class="row col-sm-12">
                                                        <div class="input-group input-group-navbar col-sm-12">
                                                            <select class=" form-control custom-select" v-model="role">
                                                              
                                                              <option v-for = "role in permissao" :value="role.id">{{role.nome }}</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class=" row col-sm-12">
                                                       <div class="input-group input-group-navbar col-sm-6"><input type="password" class="form-control" placeholder="Passord" v-model="passwords"></div>

                                                        <div class="input-group input-group-navbar col-sm-6"><input type="password" class="form-control" placeholder="Confirmar password"  v-model="confirm_password">
                                                        </div>
                                                    </div>

                                                  </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-pill btn-secondary" data-dismiss="modal" @click="effect">Close</button>
                                                    <button type="button" class="btn btn-pill btn-secondary" @click="register">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
    </div>
</div>
</template>

<script type="text/javascript">

export default{



























     props: ['roles'],
    data () {
        return {
            page: 1,
            perPage: 2,
            usersget:[],
            pages: [],   
            loading:false,
            name:'',
            role:'',
            emails:'',
            passwords:'',
            confirm_password:'',
            url:'',
            req:[],
            error:{'class':"alert alert-warning alert-dismissible","alert":"Alerta","message":"Novo usuari"},
            permissao:this.roles
        }
    },
   
    methods:{
        setPages () {
            let numberOfPages = Math.ceil(this.usersget.length / this.perPage);
            for (let index = 1; index <= numberOfPages; index++) {
                console.log(this.usersget.length);
                this.pages.push(index);
            }
        },
        paginate (posts) {
            let page = this.page;
            let perPage = this.perPage;
            let from = (page * perPage) - perPage;
            let to = (page * perPage);
            return  posts.slice(from, to);
        },
        fechUsers(){
                    this.loading = true;
                    this.usersget = [];
                    this.url = "/all/users";
                    axios.get(this.url).then((response)=>{
                       console.log(response.data);
                       const data = response.data;
                       this.usersget = data;
                       this.loading = false; 
                    });
                },
                effect(){
                     this.error = {'class':"alert alert-warning alert-dismissible","alert":"Alerta","message":"Novo usuario"}
                },

                register(){

                    this.req = {'name':this.name,'email':this.emails,'password':this.passwords,'password_confirmation':this.confirm_password,'role_id':this.role},
                    axios.post('/register',
                    this.req,
                    this.role,
                    this.emails,
                    this.passwords
                    
                        ).then((response) =>{
                            console.log(response);
                            this.fechUsers();
                            $('#newUser').modal('hide');
                                this.name='';
                                this.role='';
                                this.emails='';
                                this.passwords='';
                                this.confirm_password='';

                            }).catch((err)=>{
                                this.error.message =(err.response.data.errors.name?", "+err.response.data.errors.name:'') + (err.response.data.errors.email?", "+err.response.data.errors.email:'')+(err.response.data.errors.password? ", "+err.response.data.errors.password:'')+(err.response.data.errors.role_id?err.response.data.errors.role_id:'');
                                this.error.class = "alert alert-danger alert-dismissible";
                                this.error.alert = "Erro";
                                console.log(err.response);
                            });
                },

                update(id){
                    console.log(id);
                }
    },
      computed: {
        displayedPosts () {
            return this.paginate(this.usersget);
        }
    },
   
    watch: {
        posts () {
            this.setPages();
        }
    },
    created(){
        this.fechUsers();
    },
    filters: {
        trimWords(value){
            return value.split(" ").splice(0,20).join(" ") + '...';
        }
    }
}

</script>
**/