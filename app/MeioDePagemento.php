<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeioDePagemento extends Model
{
    protected $fillable = [
        'nome'
     ];
}
