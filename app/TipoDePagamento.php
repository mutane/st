<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDePagamento extends Model
{
     protected $fillable = [
        'nome',
     ];
}
