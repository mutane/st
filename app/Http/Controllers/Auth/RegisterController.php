<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /**
       if (Auth::check() && (Auth::user()->role->id == 1)) {
            $this->redirectTo = route('admin');
        } elseif ( Auth::check() && (Auth::user()->role->id == 2)){
            $this->redirectTo = route('gestor');
        }elseif ( Auth::check() && (Auth::user()->role->id == 3)){
            $this->redirectTo = route('caixa');
        }elseif ( Auth::check() && (Auth::user()->role->id == 4)){
            $this->redirectTo = route('cozinha');
        }elseif ( Auth::check() && (Auth::user()->role->id == 5)){
            $this->redirectTo = route('entregador');
        }elseif ( Auth::check() && (Auth::user()->role->id == 6)){
            $this->redirectTo = route('tranportador');
        }elseif ( Auth::check() && (Auth::user()->role->id == 7)){
            $this->redirectTo = route('cliente');
        }
        $this->middleware('guest');**/
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role_id' => ['required'], 
        ]);
    }

     protected function validatorupdate(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'role_id' => ['required'], 
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::Create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role_id' => $data['role_id'],
            'password' => Hash::make($data['password']),
        ]);
    }


    protected function update(Request $request)
    {
         Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'role_id' => ['required'], 
        ]);

         
    }






    public function getUsers(Request $request)
    {
       return  response()->json(User::with('role')->paginate(10)); 
    }

     public function getUser($id)
    {
        return  response()->json(User::where(['id'=>$id])->with('role')->get()); 
    }
}

/***
<template>
   <div  class="col-sm-12">
  <div class="offset"> 
<div class="col-12 col-xl-12 ">
        <div class="card shadow-sm">
            <div class="card-header">
                <button type="button" class="btn btn-pill btn-primary z-depth-2" data-toggle="modal" data-target="#newUser" @click="effect">Novo usuario</button>
            </div>
            <table class="table table-striped">
                <thead>
                                            <tr>
                                                <th style="width:40%;">Name</th>
                                                <th style="width:25%">Phone Number</th>
                                                <th class="d-none d-md-table-cell" style="width:25%">Date of Birth</th>
                                                <th colspan="2">Actions</th>
                                            </tr>
                </thead>
                <tbody >
                                            <tr v-for="user in displayedusersget">
                                                <td><img src="img/avatars/avatar-5.jpg" width="30" height="30" class="rounded-circle mr-2" alt="Avatar">{{ user.name }}</td>
                                                <td>{{user.role.nome}}</td>
                                                <td class="d-none d-md-table-cell">{{ user.email}}</td>
                                                <td class="table-action">
                                                    <a href="#">
                                                        <button class="btn btn-pill btn-warning z-depth-1" @click="update(user.id)"> Edit</button>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#"><button class="btn btn-pill btn-danger z-depth-1">Delete</button></a>
                                                </td>
                                            </tr>
                </tbody>
            </table>
               <div class="card-footer">
                    <nav aria-label="Page navigation example col">
                    <ul class="pagination">
                        <li class="page-item">
                            <button type="button" class="page-link" v-if="page != 1" @click="page--"> Previous </button>
                        </li>
                        <li class="page-item row">
                            <button type="button" class="page-link" v-for="pageNumber in pages.slice(page-1, page+5)" @click="page = pageNumber"> {{pageNumber}} </button>
                        </li>
                        <li class="page-item">
                            <button type="button" @click="page++" v-if="page < pages.length" class="page-link"> Next </button>
                        </li>
                    </ul>
                </nav> 
               </div>
        </div>
         <div class="modal fade " id="newUser" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-md animated zoomIn speeder" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Usuario</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="effect"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                                <div class="modal-body m-3">
                                                     <div :class="error.class" role="alert"><div class="alert-message"><strong>{{ error.alert }}!</strong>  {{ error.message }}</div></div>
                                                  <form>
                                                    <div class=" row col-sm-12">
                                                       <div class="input-group input-group-navbar col-sm-6"><input type="text" class="form-control" placeholder="Nome" v-model="name" autocomplete="name">
                                                       </div>

                                                        <div class="input-group input-group-navbar col-sm-6"><input type="email" class="form-control" placeholder="Email"  v-model="emails">
                                                        </div>
                                                    </div>
                                                    <div class="row col-sm-12">
                                                        <div class="input-group input-group-navbar col-sm-12">
                                                            
                                                            <select class=" form-control custom-select" v-model="role">
                                                              <option v-for = "role in permissao" :value="role.id">{{role.nome }}</option>

                                                            </select>
                                                        </div>
                                                        
                                                    </div>
                                                     <div class=" row col-sm-12">
                                                       <div class="input-group input-group-navbar col-sm-6"><input type="password" class="form-control" placeholder="Passord" v-model="passwords" :disable ='disabled'></div>

                                                        <div class="input-group input-group-navbar col-sm-6"><input type="password" class="form-control" placeholder="Confirmar password"  v-model="confirm_password">
                                                        </div>
                                                    </div>

                                                  </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-pill btn-secondary" data-dismiss="modal" @click="effect">Close</button>
                                                    <button type="button" class="btn btn-pill btn-secondary" @click="register">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
        </div>
    </div> 
  </div>
</div>


</template>
<script type="text/javascript">
    export default{
         props: ['roles'],
         data () {
        return {
            page: 1,
            perPage: 9,
            pages: [], 
            page: 1,
            perPage: 5,
            usersget:[],
            pages: [],   
            loading:false,
            name:'',
            role:'',
            emails:'',
            passwords:'',
            confirm_password:'',
            url:'',
            disabled:'true',
            req:[],
            id:null,
            error:{'class':"alert alert-warning alert-dismissible","alert":"Alerta","message":"Novo usuario"},
            permissao:this.roles     
        }
    },
    methods:{
        getusersget () {   
                  this.loading = true;
                    this.usersget = [];
                    this.url = "/all/users";
                    axios.get(this.url).then((response)=>{
                       console.log(response.data);
                       const data = response.data;
                       this.usersget = data;
                       this.loading = false; 
                    });
                    
        },
        setPages () {
            this.pages= [];
            let numberOfPages = Math.ceil(this.usersget.length / this.perPage);
            for (let index = 1; index <= numberOfPages; index++) {
                this.pages.push(index);
            }
        },
        paginate (usersget) {
            let page = this.page;
            let perPage = this.perPage;
            let from = (page * perPage) - perPage;
            let to = (page * perPage);
            return  usersget.slice(from, to);
        },
        effect(){
                     this.error = {'class':"alert alert-warning alert-dismissible","alert":"Alerta","message":"Novo usuario"}
                },
         register(){

                    this.req = {'id':this.id,'name':this.name,'email':this.emails,'password':this.passwords,'password_confirmation':this.confirm_password,'role_id':this.role},
                    axios.post('/register',this.req).then((response) =>{
                            //console.log(response);
                            // displayedusersget(); //displayedusersget();
                            $('#newUser').modal('hide');
                                this.name='';
                                this.role='';
                                this.emails='';
                                this.passwords='';
                                this.confirm_password='';
                                 this.getusersget ();
                                 this.setPages();
                                //this.paginate(this.usersget);

                            }).catch((err)=>{
                                this.error.message =(err.response.data.errors.name?", "+err.response.data.errors.name:'') + (err.response.data.errors.email?", "+err.response.data.errors.email:'')+(err.response.data.errors.password? ", "+err.response.data.errors.password:'')+(err.response.data.errors.role_id?err.response.data.errors.role_id:'');
                                this.error.class = "alert alert-danger alert-dismissible";
                                this.error.alert = "Erro";
                                console.log(err.response);
                            });
                },

                update(id){
                     $('#newUser').modal().show();
                      this.url = "/"+id+"/user";
                    axios.get(this.url).then((response)=>{
                       console.log(response.data);
                       const data = response.data[0];
                       this.name=data.name;this.role = data.role.id; this.emails=data.email;this.id=data.id;
                    });
                }

    },
    computed: {
        displayedusersget () {
            return this.paginate(this.usersget);
        }
    },
    watch: {
        usersget () {
            this.setPages();
        }
    },
    created(){
        this.getusersget();
    },
    filters: {
        trimWords(value){
            return value.split(" ").splice(0,20).join(" ") + '...';
        }
    }
    }
</script>***/