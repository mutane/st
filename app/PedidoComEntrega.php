<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoComEntrega extends Model
{
   protected $fillable = [
        'entidade','data','tributacao','subtotal','total','frete','items','cliente','usuario','endereco','cidade','bairro','observacao'
     ];
}
