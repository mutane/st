<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entregador extends Model
{
    protected $fillable = [
        'nome' ,'endereco' ,'bairro','cidade','documento','contacto','email','complemento','numero','inserido_por','alterado_por','pessoa_fisica'
     ];
}
