<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDeTransacao extends Model
{
    protected $fillable = [
        'nome',
     ];
}
