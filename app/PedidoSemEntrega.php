<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoSemEntrega extends Model
{
    protected $fillable = [
        'entidade','data','tributacao','subtotal','total','frete','items','cliente','usuario'
     ];
}
