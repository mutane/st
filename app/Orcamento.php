<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orcamento extends Model
{
    protected $fillable = [
        'entidade','estado','situacao_em','total_final','cliente','valido_ate','venda_id','taxa','taxa_porcento','funcionario','aprovado_em','recusado_em','vedido_em','expirado_em','frete','email_enviado','enviado_por','email_enviado_em'
     ];
}
