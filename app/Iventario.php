<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iventario extends Model
{
     protected $fillable = [
        'data', 'valor', 'concluido',
     ];
}
