<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $fillable = [
        'nome', 'descricao'
     ];

    public function user()
    {


      Auth::user();
        return $this->hasMany('App\User');
    }
}
