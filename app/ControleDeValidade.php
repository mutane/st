<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControleDeValidade extends Model
{
    protected $fillable = [
      'validade','dias_restantes','data_de_baixa','baxado_por','fornecedor_id'
     ];
}
