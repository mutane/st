<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{

	protected $fillable = [
        'nome' ,'endereco' ,'bairro','cidade','documento','contacto','email','complemento','numero','inserido_por','alterado_por','pessoa_fisica'
     ];


     public function produto()
    {
        return $this->hasMany('App\Produto'
    }
}
