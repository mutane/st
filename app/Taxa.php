<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxa extends Model
{
    protected $fillable = [
        'nome', 'valor'
     ];

     public function produto()
    {
        return $this->hasMany('App\Produto');
    }
}
