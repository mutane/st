<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    protected $fillable = [
        'uuid','tipo','data','codigo','cliente_id','produto_id','quantidade','val_unitario','subtotal','desconto','total_item','perc_de_comissao','valor_de_comissao','vendedor','meio_de_pagamento','custo','lucro','taxa_incluida','funcionario'
     ];
}
