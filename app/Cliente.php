<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
     protected $fillable = [
        'nome','data_de_nascimento','documento','endereco','cidade','bairro','sexo','contacto','email','debito','credito','tem_debito','status','informacoes','inserido_por','alterado_por','limite_debito','entregador','transportador','ultima_visita','imagem'
     ];
}
