<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = [
        	'descricao'
            ,'codigo'
           ,'estoque_actual'
           ,'estoque_disponivel'
           ,'estoque_reservado'
            ,'preco'
            //relation'ship
           ,'categoria_id'
           ,'unidade_id'
           ,'marca_id'
           ,'fornecedor_id'
           ,'taxa_id'
            //fimdos relation'ships

            ,'cod_referencia'
           ,'estoque_min'
           ,'estoque_max'
            ,'repor'
            ,'premio'
            ,'tributacao'
            ,'peso_liquido'
            ,'peso_solido'
            ,'comissao'
            ,'promocao'
            ,'data_inicial'
            ,'data_final'
            ,'preco'
         	 ,'status'
            ,'kit_kombo'
            ,'image'
           ,'pontos_necessarios'
     ];


     public function taxa()
    {
        return $this->belongsTo('App\Taxa');
    }

    public function marca()
    {
        return $this->belongsTo('App\Marca');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }

    public function unidade()
    {
        return $this->belongsTo('App\Unidade');
    }

    public function fornecedor()
    {
        return $this->belongsTo('App\Fornecedor');
    }

}
