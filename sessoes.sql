-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Sep 15, 2020 at 10:20 AM
-- Server version: 8.0.18
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sessoes`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_de_nascimento` date NOT NULL,
  `documento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contacto` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debito` double NOT NULL,
  `credito` double NOT NULL,
  `tem_debito` tinyint(1) NOT NULL,
  `estado` int(11) NOT NULL,
  `informacoes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inserido_por` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alterado_por` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `limite_debito` double NOT NULL,
  `entregador` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transportador` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ultima_visita` timestamp NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `controle_de_validades`
--

DROP TABLE IF EXISTS `controle_de_validades`;
CREATE TABLE IF NOT EXISTS `controle_de_validades` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `validade` timestamp NOT NULL,
  `dias_restantes` int(11) NOT NULL,
  `data_de_baixa` timestamp NOT NULL,
  `baxado_por` int(11) NOT NULL,
  `fornecedor_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `costumers`
--

DROP TABLE IF EXISTS `costumers`;
CREATE TABLE IF NOT EXISTS `costumers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `costumers`
--

INSERT INTO `costumers` (`id`, `name`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'Alisha', 'brandt.herzog@gmail.com', '852.593.5192', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(2, 'Fermin', 'garry.hauck@bogan.com', '508-580-4883 x4990', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(3, 'Adolphus', 'tswaniawski@weimann.biz', '260-326-1535 x5746', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(4, 'Madeline', 'kautzer.alexandrine@yahoo.com', '473.839.0204', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(5, 'Nannie', 'imaggio@yahoo.com', '1-690-280-3981 x6498', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(6, 'Abner', 'pbaumbach@koss.com', '+1-916-477-6664', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(7, 'Madisen', 'mac38@prohaska.com', '+1.221.704.6168', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(8, 'Clovis', 'piper93@heller.org', '(325) 569-2177 x2542', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(9, 'Earline', 'dejuan.bernhard@gmail.com', '1-407-449-3073 x9459', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(10, 'Nash', 'mollie.gleichner@gmail.com', '943.375.0985 x4462', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(11, 'Romaine', 'dvonrueden@hotmail.com', '(775) 309-8964', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(12, 'Juston', 'hahn.cathrine@gmail.com', '1-504-429-6635', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(13, 'David', 'oconner.elian@hartmann.com', '+1-321-261-2361', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(14, 'Flavie', 'horacio.jacobi@gmail.com', '436.809.9638', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(15, 'Ryan', 'jessie71@stark.com', '(546) 562-3462', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(16, 'Sonny', 'maxime.howell@yahoo.com', '(872) 657-7120', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(17, 'Andreanne', 'sporer.kiera@gmail.com', '482.537.8430 x14210', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(18, 'Monty', 'rosemarie.roberts@satterfield.com', '351-521-5261', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(19, 'Karianne', 'iharber@yahoo.com', '613-394-8622 x5776', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(20, 'Penelope', 'dabernathy@ondricka.biz', '1-887-640-9984', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(21, 'Trevor', 'aurelia.lesch@schaden.com', '+1 (253) 756-0550', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(22, 'Marjorie', 'goyette.orpha@gmail.com', '240.682.9935 x347', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(23, 'Berry', 'germaine13@hotmail.com', '806.808.6524', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(24, 'Angelita', 'lupe.nicolas@welch.net', '1-481-498-3449', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(25, 'Flossie', 'uratke@baumbach.org', '312-349-7833 x868', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(26, 'Aylin', 'kari.miller@yahoo.com', '529-367-2283 x6020', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(27, 'London', 'major.orn@yahoo.com', '1-358-359-1500', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(28, 'Jayne', 'ldare@armstrong.biz', '(449) 472-4906 x606', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(29, 'Anderson', 'larkin.pietro@gaylord.com', '1-843-941-6885', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(30, 'Clifton', 'leland34@gorczany.com', '+1.259.282.9845', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(31, 'Karolann', 'uterry@walter.info', '485.508.4264 x17937', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(32, 'Juston', 'wunsch.carlotta@zulauf.com', '(829) 280-9923 x058', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(33, 'Brielle', 'izemlak@gmail.com', '(484) 966-8250 x964', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(34, 'Anna', 'kyra22@beer.net', '701.583.7399 x8526', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(35, 'Eldridge', 'claude32@yahoo.com', '717.573.3899 x36550', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(36, 'Davon', 'friesen.dominic@berge.org', '227.948.3219', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(37, 'Hillary', 'hfeil@cassin.com', '761-465-2029', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(38, 'Gillian', 'erling.kunde@hayes.com', '943-649-0882 x565', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(39, 'Kari', 'lesch.kyleigh@gmail.com', '1-813-327-0805 x942', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(40, 'Elizabeth', 'qkohler@muller.info', '1-548-487-6228', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(41, 'Lola', 'jayce.thompson@hotmail.com', '893-781-9828', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(42, 'Carissa', 'macejkovic.molly@hotmail.com', '+14194052207', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(43, 'Burley', 'mlangosh@hotmail.com', '1-851-504-5236', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(44, 'Wyatt', 'yoshiko51@hotmail.com', '(815) 517-2854', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(45, 'Floyd', 'nblanda@hotmail.com', '1-538-523-7214', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(46, 'Bartholome', 'tyrese13@nikolaus.com', '882-937-3941 x985', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(47, 'Krystel', 'lboehm@hotmail.com', '+1.535.373.2520', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(48, 'Garth', 'kgutmann@labadie.com', '(303) 956-1645', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(49, 'Camron', 'arlie.dickinson@beier.com', '1-458-693-0300 x2632', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(50, 'Bert', 'larson.noe@olson.com', '(518) 899-9481 x330', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(51, 'Chad', 'yreilly@feil.com', '1-695-378-3803', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(52, 'Danielle', 'oren.purdy@bernier.info', '635.299.6058 x815', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(53, 'Bethel', 'lang.eudora@yahoo.com', '817-252-4969 x5382', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(54, 'Adriana', 'clementina.huel@yahoo.com', '926.964.0161 x163', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(55, 'Danial', 'heathcote.korbin@schuppe.com', '594-632-0401', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(56, 'Dora', 'terrence10@friesen.com', '+17564036437', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(57, 'Norris', 'ashton97@hotmail.com', '1-854-941-8221', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(58, 'Lindsey', 'deborah76@gmail.com', '858-998-4511 x803', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(59, 'Lauretta', 'initzsche@wilderman.com', '954.314.9885 x927', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(60, 'Victoria', 'layne.brown@yahoo.com', '+1 (385) 992-4757', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(61, 'Sabrina', 'mireya82@champlin.biz', '848.563.0861 x13062', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(62, 'Sylvan', 'stehr.jordane@schowalter.biz', '1-235-684-1819 x725', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(63, 'Stephon', 'bayer.selina@muller.com', '883-593-7451', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(64, 'Patsy', 'schiller.aglae@gmail.com', '(427) 350-4057 x9547', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(65, 'Margarita', 'kirk.schneider@mann.com', '847.639.9763', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(66, 'Susanna', 'oswaldo.kuphal@lind.com', '+17835537734', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(67, 'Manuel', 'qebert@wisoky.net', '640.707.7871 x479', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(68, 'Antonina', 'fkris@gmail.com', '+1-531-980-5485', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(69, 'Hanna', 'uherzog@johnson.com', '+1-867-885-0428', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(70, 'Leanna', 'kylee.bednar@hotmail.com', '979-803-0429', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(71, 'Ardith', 'jschneider@shields.org', '(897) 284-0221', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(72, 'Nikko', 'kreichert@hotmail.com', '+1 (491) 824-9561', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(73, 'Aiyana', 'florian88@wehner.biz', '620-523-5916 x65025', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(74, 'Forrest', 'von.geovanni@hotmail.com', '704-507-5293 x3172', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(75, 'Zechariah', 'shawna86@yahoo.com', '+1-742-247-2884', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(76, 'Forest', 'rossie27@kertzmann.com', '(751) 390-6808 x129', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(77, 'Dorcas', 'hessel.judson@waelchi.com', '573-635-7803 x119', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(78, 'Elliott', 'dolly.brakus@trantow.net', '638-485-2567', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(79, 'Lamar', 'alize.schmidt@wiza.info', '+17746392431', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(80, 'Greyson', 'mallie.labadie@hotmail.com', '+1-423-893-6088', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(81, 'Dawn', 'eli.abshire@gmail.com', '1-301-390-1610 x0039', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(82, 'Romaine', 'estel.kihn@hotmail.com', '+1-612-821-7735', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(83, 'Jensen', 'alba.morissette@yahoo.com', '413.960.9467 x29717', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(84, 'Lonie', 'angie.monahan@gmail.com', '1-445-605-1852 x4016', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(85, 'Aaron', 'nico.skiles@yahoo.com', '337-316-9445', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(86, 'Tobin', 'ozieme@harber.com', '849-257-1030', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(87, 'Rupert', 'hamill.kiley@gmail.com', '678-819-7351 x420', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(88, 'Stanton', 'ludwig65@gmail.com', '1-332-901-0585 x592', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(89, 'Claude', 'deron81@rath.com', '725-837-0578 x928', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(90, 'Morton', 'vida14@hotmail.com', '(723) 250-8810', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(91, 'Geovany', 'hal.bernhard@gmail.com', '+1-409-420-6723', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(92, 'Kellen', 'cummings.cleora@yahoo.com', '740-413-1277 x12387', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(93, 'Brady', 'adam50@yahoo.com', '(221) 682-2655', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(94, 'Hailee', 'moriah.ritchie@klocko.net', '969.563.0328 x3257', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(95, 'Deborah', 'brycen.oconnell@emmerich.biz', '1-674-713-2218 x5438', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(96, 'Sherman', 'remington.oconner@hotmail.com', '1-839-308-4116', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(97, 'Clarissa', 'tressie.sipes@schamberger.org', '921.406.7883', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(98, 'Jared', 'hyatt.nettie@langworth.biz', '747.904.1850 x289', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(99, 'Lavina', 'brooklyn.kirlin@hotmail.com', '1-575-532-1269 x51095', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(100, 'Gerson', 'xgleason@hotmail.com', '467.932.3383 x824', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(101, 'Raymond', 'deanna.heathcote@yahoo.com', '803.862.1197 x9934', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(102, 'Ross', 'klemke@walsh.biz', '+1 (270) 328-2483', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(103, 'Geraldine', 'karlee.kunde@hotmail.com', '1-332-909-4639 x6692', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(104, 'Kacie', 'larkin.rafaela@hyatt.com', '226-714-4983', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(105, 'Raven', 'sherwood.rippin@hilpert.com', '1-729-842-9180 x49017', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(106, 'Liana', 'myrtice68@gmail.com', '423-395-0432 x0177', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(107, 'Dexter', 'fay.jaeden@johnson.com', '1-570-422-7494 x987', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(108, 'Reanna', 'geovanni08@gmail.com', '406.880.7448 x23023', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(109, 'Jettie', 'fcormier@buckridge.net', '+1-805-383-0556', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(110, 'Adriana', 'hagenes.nova@heathcote.com', '(227) 404-8085', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(111, 'Arvilla', 'herman.keon@harvey.com', '918-704-7869', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(112, 'Dameon', 'britchie@yahoo.com', '915.471.7409', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(113, 'Bettie', 'harber.luis@yahoo.com', '432.614.8907 x4731', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(114, 'Graciela', 'candace.cartwright@hotmail.com', '(750) 277-3917 x183', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(115, 'Cordelia', 'lina.bins@konopelski.com', '+1-206-495-6633', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(116, 'Kip', 'linnie98@kemmer.info', '980.290.9989', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(117, 'Brittany', 'schmidt.carli@yahoo.com', '1-367-479-9649 x539', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(118, 'Damien', 'renner.cristian@hotmail.com', '(616) 207-3333', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(119, 'Darrell', 'kjaskolski@yahoo.com', '1-308-464-4646 x23231', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(120, 'Reid', 'ksenger@maggio.com', '515.424.1272 x7755', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(121, 'Rosanna', 'hartmann.celia@lockman.info', '+1-835-374-9338', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(122, 'Aron', 'lupe.kuphal@yahoo.com', '+1 (925) 630-6235', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(123, 'Favian', 'jody76@steuber.info', '+1.414.506.2560', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(124, 'Lera', 'sophia.mraz@prosacco.net', '543.856.5184 x22235', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(125, 'Jerel', 'amante@gmail.com', '1-362-820-1964 x5257', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(126, 'Quinton', 'farrell.kaitlyn@hotmail.com', '579.929.5846', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(127, 'Dax', 'stanford52@yahoo.com', '1-685-750-3354', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(128, 'Macie', 'houston.halvorson@hotmail.com', '1-563-709-9792 x979', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(129, 'Silas', 'kari98@gmail.com', '+18303102653', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(130, 'Alverta', 'dale.dach@yahoo.com', '358-221-7659 x9439', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(131, 'Pat', 'khoppe@hotmail.com', '719-384-5283', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(132, 'Florian', 'ksauer@gmail.com', '554-678-3136 x135', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(133, 'Fanny', 'rhianna36@bradtke.net', '(302) 640-5673', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(134, 'Hattie', 'pbednar@hotmail.com', '(591) 253-0915 x994', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(135, 'Liliane', 'queenie.nolan@yahoo.com', '+1-408-739-2615', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(136, 'Ricardo', 'lesly40@gmail.com', '(443) 204-5358 x35245', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(137, 'Annetta', 'marjorie.wisoky@hotmail.com', '867.463.4743 x0277', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(138, 'Vincenza', 'jeanne40@gmail.com', '+1-926-796-4726', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(139, 'Karina', 'adolf.gaylord@gmail.com', '431-450-6171', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(140, 'Angelo', 'trisha38@hotmail.com', '(872) 785-1781 x70880', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(141, 'Gustave', 'emie76@gmail.com', '(662) 761-4079', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(142, 'Keanu', 'pearl69@yahoo.com', '1-324-479-6401 x16322', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(143, 'Sean', 'ryley59@carroll.com', '858.553.8349 x977', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(144, 'Kirk', 'wheaney@gusikowski.com', '+1-278-839-5277', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(145, 'Toney', 'tpouros@hotmail.com', '(689) 230-2942', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(146, 'Carlos', 'frederique81@hotmail.com', '(534) 962-7437', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(147, 'Viola', 'eriberto05@hotmail.com', '+19172017392', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(148, 'Irma', 'ikuhn@heller.com', '(717) 244-4274 x88657', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(149, 'Isabella', 'tboyer@hotmail.com', '296-722-9989', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(150, 'Jacquelyn', 'melyna46@orn.org', '(286) 228-8025 x16243', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(151, 'Adeline', 'ellen82@yahoo.com', '821-409-2149', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(152, 'Crawford', 'ocorkery@tremblay.com', '+1-868-263-3394', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(153, 'Arnulfo', 'whaag@ebert.org', '+1 (369) 902-5090', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(154, 'Makenzie', 'grant.monserrat@donnelly.com', '1-472-847-4626 x3324', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(155, 'Lura', 'arjun.wiza@bednar.biz', '(367) 527-3195', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(156, 'Eliane', 'iwhite@hotmail.com', '462.949.7383', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(157, 'Jasmin', 'jones.berneice@gmail.com', '513.225.7705 x1885', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(158, 'Yessenia', 'ortiz.lindsay@yahoo.com', '257-834-9850 x89375', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(159, 'Ardella', 'daugherty.keenan@sawayn.info', '(613) 575-8978 x8000', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(160, 'Sedrick', 'raegan97@hotmail.com', '858-281-8167', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(161, 'Ashton', 'rico.schoen@hotmail.com', '1-448-529-7083', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(162, 'Nikki', 'tlubowitz@yahoo.com', '445.671.2413 x495', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(163, 'Kelton', 'reilly.jerrold@yahoo.com', '239.804.5876 x832', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(164, 'Devan', 'deontae.moore@hotmail.com', '+1-356-973-6204', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(165, 'Edgardo', 'pablo.conroy@yahoo.com', '(394) 647-3990 x622', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(166, 'Eliezer', 'matilda.legros@yahoo.com', '571-425-6613', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(167, 'Neha', 'feeney.delphia@roob.com', '1-798-559-6404', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(168, 'Blaise', 'jayme18@gmail.com', '+1 (761) 863-3490', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(169, 'Jefferey', 'dedric78@yahoo.com', '(373) 386-3011 x3584', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(170, 'Miller', 'hipolito14@kuhic.com', '1-810-885-5514 x20436', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(171, 'Fredy', 'queen.boyle@fadel.com', '+1.727.861.6309', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(172, 'Autumn', 'heaney.sylvia@luettgen.com', '347-483-8573 x6047', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(173, 'Kianna', 'leonor24@gmail.com', '+16268706177', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(174, 'Arden', 'edna99@yahoo.com', '(719) 802-7588 x49244', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(175, 'Josie', 'shayna48@graham.org', '(795) 219-9183 x70471', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(176, 'Cassie', 'dane24@hotmail.com', '429.345.2578 x25648', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(177, 'Brady', 'lonnie00@corkery.info', '490.952.1901 x229', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(178, 'Vicenta', 'hayes.muriel@smitham.com', '1-297-997-6724 x421', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(179, 'Nasir', 'mglover@spinka.com', '1-572-754-9006', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(180, 'Lavon', 'jacobson.lillie@yahoo.com', '886-704-2846', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(181, 'Felton', 'stacey45@gmail.com', '(709) 467-1951 x412', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(182, 'Macie', 'bohara@wiegand.biz', '(676) 956-9524 x300', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(183, 'Jarred', 'dean52@oberbrunner.net', '709.406.7363 x36111', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(184, 'Curt', 'benedict.ullrich@wehner.com', '782-593-9685 x4458', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(185, 'Candida', 'catharine.rempel@hotmail.com', '520-506-4552 x81708', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(186, 'Diamond', 'piper.murazik@blick.com', '+1 (925) 229-8055', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(187, 'Whitney', 'madisyn59@gmail.com', '(276) 580-4826 x053', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(188, 'Domingo', 'thalia.stiedemann@gmail.com', '738.905.0391', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(189, 'Joanne', 'hermiston.shania@strosin.com', '+1 (864) 406-6244', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(190, 'Zelma', 'rcremin@crooks.com', '639.844.9330', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(191, 'Ophelia', 'jeffrey88@yahoo.com', '(356) 616-8020', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(192, 'Onie', 'nolan.arne@hotmail.com', '1-689-836-8370', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(193, 'Zoey', 'wmarquardt@goyette.biz', '1-983-837-0462', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(194, 'Modesto', 'aauer@hotmail.com', '1-778-633-8185', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(195, 'Kaylie', 'eulah48@yahoo.com', '(665) 801-1295', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(196, 'Jakob', 'padberg.virgie@gmail.com', '(740) 460-9434 x88877', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(197, 'Lurline', 'pspencer@oberbrunner.com', '409-826-9004 x685', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(198, 'Kathlyn', 'dickinson.carlos@barton.com', '(290) 877-2025 x7116', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(199, 'Ethelyn', 'avery.stracke@walsh.com', '387.375.7493 x9298', '2020-07-14 07:36:08', '2020-07-14 07:36:08'),
(200, 'Yesenia', 'reed.dooley@yahoo.com', '(340) 984-5093', '2020-07-14 07:36:08', '2020-07-14 07:36:08');

-- --------------------------------------------------------

--
-- Table structure for table `entregadors`
--

DROP TABLE IF EXISTS `entregadors`;
CREATE TABLE IF NOT EXISTS `entregadors` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contacto` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complemento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` int(11) NOT NULL,
  `inserido_por` int(11) NOT NULL,
  `alterado_por` int(11) NOT NULL,
  `pessoa_fisica` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `estados`
--

DROP TABLE IF EXISTS `estados`;
CREATE TABLE IF NOT EXISTS `estados` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fornecedors`
--

DROP TABLE IF EXISTS `fornecedors`;
CREATE TABLE IF NOT EXISTS `fornecedors` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contacto` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complemento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` int(11) NOT NULL,
  `inserido_por` int(11) NOT NULL,
  `alterado_por` int(11) NOT NULL,
  `pessoa_fisica` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iventarios`
--

DROP TABLE IF EXISTS `iventarios`;
CREATE TABLE IF NOT EXISTS `iventarios` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `valor` double NOT NULL,
  `concluido` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcas`
--

DROP TABLE IF EXISTS `marcas`;
CREATE TABLE IF NOT EXISTS `marcas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `marca` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meio_de_pagementos`
--

DROP TABLE IF EXISTS `meio_de_pagementos`;
CREATE TABLE IF NOT EXISTS `meio_de_pagementos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_06_30_142513_create_roles_table', 1),
(5, '2020_07_04_143919_create_unidades_table', 1),
(6, '2020_07_04_144257_create_iventarios_table', 1),
(7, '2020_07_04_144925_create_taxas_table', 1),
(8, '2020_07_04_145304_create_marcas_table', 1),
(9, '2020_07_04_145550_create_categorias_table', 1),
(10, '2020_07_04_145857_create_produtos_table', 1),
(11, '2020_07_04_195549_create_fornecedors_table', 1),
(12, '2020_07_04_201625_create_controle_de_validades_table', 1),
(13, '2020_07_04_204321_create_pais_table', 1),
(14, '2020_07_05_152416_create_meio_de_pagementos_table', 1),
(15, '2020_07_05_152752_create_tipo_de_pagamentos_table', 1),
(16, '2020_07_05_152855_create_estados_table', 1),
(17, '2020_07_05_153107_create_vendas_table', 1),
(18, '2020_07_05_160014_create_clientes_table', 1),
(19, '2020_07_05_164112_create_tipo_de_transacaos_table', 1),
(20, '2020_07_05_165215_create_orcamentos_table', 1),
(21, '2020_07_05_171116_create_pedido_sem_entregas_table', 1),
(22, '2020_07_05_181100_create_pedido_com_entregas_table', 1),
(23, '2020_07_05_182334_create_entregadors_table', 1),
(24, '2020_07_09_233100_create_websockets_statistics_entries_table', 1),
(25, '2020_07_14_092458_create_costumers_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orcamentos`
--

DROP TABLE IF EXISTS `orcamentos`;
CREATE TABLE IF NOT EXISTS `orcamentos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `entidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `situacao_em` timestamp NOT NULL,
  `total_final` double NOT NULL,
  `cliente` int(11) NOT NULL,
  `valido_ate` timestamp NOT NULL,
  `venda_id` int(11) NOT NULL,
  `taxa` double NOT NULL,
  `taxa_porcento` double NOT NULL,
  `funcionario` int(11) NOT NULL,
  `aprovado_em` timestamp NOT NULL,
  `recusado_em` timestamp NOT NULL,
  `vedido_em` timestamp NOT NULL,
  `expirado_em` timestamp NOT NULL,
  `frete` double NOT NULL,
  `email_enviado` tinyint(1) NOT NULL,
  `enviado_por` int(11) NOT NULL,
  `email_enviado_em` timestamp NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
CREATE TABLE IF NOT EXISTS `pais` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pedido_com_entregas`
--

DROP TABLE IF EXISTS `pedido_com_entregas`;
CREATE TABLE IF NOT EXISTS `pedido_com_entregas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `entidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` timestamp NOT NULL,
  `tributacao` double NOT NULL,
  `subtotal` double NOT NULL,
  `total` double NOT NULL,
  `frete` double NOT NULL,
  `items` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cliente` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `endereco` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pedido_sem_entregas`
--

DROP TABLE IF EXISTS `pedido_sem_entregas`;
CREATE TABLE IF NOT EXISTS `pedido_sem_entregas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `entidade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` timestamp NOT NULL,
  `tributacao` double NOT NULL,
  `subtotal` double NOT NULL,
  `total` double NOT NULL,
  `frete` double NOT NULL,
  `items` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cliente` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
CREATE TABLE IF NOT EXISTS `produtos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estoque_actual` int(11) NOT NULL,
  `estoque_disponivel` int(11) NOT NULL,
  `estoque_reservado` int(11) NOT NULL,
  `preco` double NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `unidade_id` int(11) NOT NULL,
  `marca_id` int(11) NOT NULL,
  `fornecedor_id` int(11) NOT NULL,
  `taxa_id` int(11) NOT NULL,
  `cod_referencia` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estoque_min` int(11) NOT NULL,
  `estoque_max` int(11) NOT NULL,
  `repor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `premio` tinyint(1) NOT NULL,
  `tributacao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peso_liquido` double NOT NULL,
  `peso_solido` double NOT NULL,
  `comissao` double NOT NULL,
  `promocao` tinyint(1) NOT NULL,
  `data_inicial` timestamp NOT NULL,
  `data_final` timestamp NOT NULL,
  `status` tinyint(1) NOT NULL,
  `kit_kombo` tinyint(1) NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pontos_necessarios` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_nome_unique` (`nome`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `nome`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '', '2020-07-12 22:00:00', '2020-07-12 22:00:00'),
(2, 'Gestor', '', '2020-07-12 22:00:00', '2020-07-12 22:00:00'),
(3, 'Caixa', '', '2020-07-12 22:00:00', '2020-07-12 22:00:00'),
(4, 'Cozinha', '', '2020-07-12 22:00:05', '2020-07-12 22:00:00'),
(5, 'Entregador', '', '2020-07-12 22:09:00', '2020-07-13 05:17:14'),
(6, 'Transportador', '', '2020-07-28 22:00:24', '2020-07-12 22:00:08'),
(7, 'Cliente', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taxas`
--

DROP TABLE IF EXISTS `taxas`;
CREATE TABLE IF NOT EXISTS `taxas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_de_pagamentos`
--

DROP TABLE IF EXISTS `tipo_de_pagamentos`;
CREATE TABLE IF NOT EXISTS `tipo_de_pagamentos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_de_transacaos`
--

DROP TABLE IF EXISTS `tipo_de_transacaos`;
CREATE TABLE IF NOT EXISTS `tipo_de_transacaos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unidades`
--

DROP TABLE IF EXISTS `unidades`;
CREATE TABLE IF NOT EXISTS `unidades` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '4',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_name_unique` (`name`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nelson Alex Mutane', 'admin@admin.com', NULL, '$2y$10$OHlRvIFJBeOHEbjnNUxRYe5STsk48Ucp5iO092pHDcEKirPMAWi6K', 1, NULL, '2020-07-13 13:51:01', '2020-07-13 13:51:01'),
(14, 'ttcd', 'johndoe@native-theme.com', NULL, '$2y$10$Lzbx0QILK/rgPVfN46dhdOn9.re67FcNxMh52KYPfz5DsMpA9ZFM6', 4, NULL, '2020-07-13 14:44:04', '2020-07-13 14:44:04'),
(15, 'gffgfttrdt', 'ad@gmail.com', NULL, '$2y$10$GCQV9zcquerIJuY1bK7uE.8X.6DMZYxjeXbDVarYUtI3pYDgaTzcS', 6, NULL, '2020-07-13 14:44:39', '2020-07-13 14:44:39'),
(16, 'ytfcrr', 'admin@example.com', NULL, '$2y$10$MLDqnGUPoxG5GeYBXBPB5e6W3x5EipAMlXWOWP6nw/H6Bn1ir9KHy', 6, NULL, '2020-07-13 14:47:21', '2020-07-13 14:47:21'),
(17, 'ytrr', 'kenyon.balistreri@example.net', NULL, '$2y$10$FfIKQlx7tGKTWAXKBOPpK.X5JRP06Nctve.tuTB1CImQfcEFpCq3.', 5, NULL, '2020-07-13 14:47:55', '2020-07-13 14:47:55'),
(18, 'hv ghf gf', 'ad4@gmail.com', NULL, '$2y$10$yUIQvI3nNuAjhuS5X82xI.6ueZ/5wdFbREZZmZgmY1LNghmKaMNUi', 6, NULL, '2020-07-13 14:48:20', '2020-07-13 14:48:20'),
(19, 'Nelson Alex Mutaned', 'admin@admifn.com', NULL, '$2y$10$fAhx4bkIZrN6KkzT4UcMNOnAh8X5l5K97s/MCsU6/isazcn250t3q', 1, NULL, '2020-07-13 17:06:05', '2020-07-13 17:06:05');

-- --------------------------------------------------------

--
-- Table structure for table `vendas`
--

DROP TABLE IF EXISTS `vendas`;
CREATE TABLE IF NOT EXISTS `vendas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `data` timestamp NOT NULL,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `val_unitario` double NOT NULL,
  `subtotal` double NOT NULL,
  `desconto` double NOT NULL,
  `total_item` double NOT NULL,
  `perc_de_comissao` double NOT NULL,
  `valor_de_comissao` double NOT NULL,
  `vendedor` int(11) NOT NULL,
  `meio_de_pagamento` int(11) NOT NULL,
  `custo` double NOT NULL,
  `lucro` double NOT NULL,
  `taxa_incluida` double NOT NULL,
  `funcionario` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `websockets_statistics_entries`
--

DROP TABLE IF EXISTS `websockets_statistics_entries`;
CREATE TABLE IF NOT EXISTS `websockets_statistics_entries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `app_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peak_connection_count` int(11) NOT NULL,
  `websocket_message_count` int(11) NOT NULL,
  `api_message_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
