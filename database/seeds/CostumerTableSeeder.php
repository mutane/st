<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Costumer;

class CostumerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Costumer::truncate();

        foreach (range(1,200) as $i) {
        	Costumer::create([
        		'name' =>$faker->firstname,
        		'email' =>$faker->email,
        		'phone'=>$faker->phoneNumber
        	]);
        }
    }
}
