<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->date('data_de_nascimento');
            $table->string('documento');
            $table->string('endereco');
            $table->string('cidade');
            $table->string('bairro');
            $table->string('sexo');
            $table->integer('contacto');
            $table->string('email');
            $table->double('debito');
            $table->double('credito');
            $table->boolean('tem_debito');
            $table->integer('estado');
            $table->string('informacoes');
            $table->string('inserido_por');
            $table->string('alterado_por');
            $table->double('limite_debito');
            $table->string('entregador');
            $table->string('transportador');
            $table->timestamp('ultima_visita');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
