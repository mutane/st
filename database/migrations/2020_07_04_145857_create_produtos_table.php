<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->id();
            $table->text('descricao');
            $table->string('codigo');
            $table->integer('estoque_actual');
            $table->integer('estoque_disponivel');
            $table->integer('estoque_reservado');
            $table->double('preco');
            //relation'ship
            $table->integer('categoria_id');
            $table->integer('unidade_id');
            $table->integer('marca_id');
            $table->integer('fornecedor_id');
            $table->integer('taxa_id');
            //fimdos relation'ships

            $table->string('cod_referencia');
            $table->integer('estoque_min');
            $table->integer('estoque_max');
            $table->string('repor');
            $table->boolean('premio');
            $table->string('tributacao');
            $table->double('peso_liquido');
            $table->double('peso_solido');
            $table->double('comissao');
            $table->boolean('promocao');
            $table->timestamp('data_inicial');
            $table->timestamp('data_final');
            $table->boolean('status');
            $table->boolean('kit_kombo');
            $table->text('image');
            $table->integer('pontos_necessarios');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
