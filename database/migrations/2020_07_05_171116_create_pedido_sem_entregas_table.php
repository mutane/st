<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoSemEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_sem_entregas', function (Blueprint $table) {
            $table->id();
            $table->string('entidade');
            $table->timestamp('data');
            $table->double('tributacao');
            $table->double('subtotal');
            $table->double('total');
            $table->double('frete');
            $table->string('items');
            $table->integer('cliente');
            $table->integer('usuario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_sem_entregas');
    }
}
