<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControleDeValidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controle_de_validades', function (Blueprint $table) {
            $table->id();
            $table->timestamp('validade');
            $table->integer('dias_restantes');
            $table->timestamp('data_de_baixa');
            $table->integer('baxado_por');
            $table->integer('fornecedor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controle_de_validades');
    }
}
