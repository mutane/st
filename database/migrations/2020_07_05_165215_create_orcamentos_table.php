<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrcamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orcamentos', function (Blueprint $table) {
            $table->id();
            $table->string('entidade');
            $table->integer('estado');
            $table->timestamp('situacao_em');
            $table->double('total_final');
            $table->integer('cliente');
            $table->timestamp('valido_ate');
            $table->integer('venda_id');
            $table->double('taxa');
            $table->double('taxa_porcento');
            $table->integer('funcionario');
            $table->timestamp('aprovado_em');
            $table->timestamp('recusado_em');
            $table->timestamp('vedido_em');
            $table->timestamp('expirado_em');
            $table->double('frete');
            $table->boolean('email_enviado');
            $table->integer('enviado_por');
            $table->timestamp('email_enviado_em');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orcamentos');
    }
}
