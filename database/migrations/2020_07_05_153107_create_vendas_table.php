<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendas', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->integer('tipo');
            $table->timestamp('data');
            $table->string('codigo');
            $table->integer('cliente_id');
            $table->integer('produto_id');
            $table->integer('quantidade');
            $table->double('val_unitario');
            $table->double('subtotal');
            $table->double('desconto');
            $table->double('total_item');
            $table->double('perc_de_comissao');
            $table->double('valor_de_comissao');
            $table->integer('vendedor');
            $table->integer('meio_de_pagamento');
            $table->double('custo');
            $table->double('lucro');
            $table->double('taxa_incluida');
            $table->string('funcionario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendas');
    }
}
