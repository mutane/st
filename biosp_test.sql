-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Sep 15, 2020 at 10:19 AM
-- Server version: 8.0.18
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biosp_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `bairro`
--

DROP TABLE IF EXISTS `bairro`;
CREATE TABLE IF NOT EXISTS `bairro` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `benificiario`
--

DROP TABLE IF EXISTS `benificiario`;
CREATE TABLE IF NOT EXISTS `benificiario` (
  `id` int(11) NOT NULL,
  `ref` varchar(45) DEFAULT NULL,
  `n_visita` int(11) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `data_atendimento` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provincia_id` int(11) NOT NULL,
  `bairro_id` int(11) NOT NULL,
  `sexo_id` int(11) NOT NULL,
  `objectivo_da_visita_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_benificiario_provincia_idx` (`provincia_id`),
  KEY `fk_benificiario_bairro1_idx` (`bairro_id`),
  KEY `fk_benificiario_sexo1_idx` (`sexo_id`),
  KEY `fk_benificiario_objectivo_da_visita1_idx` (`objectivo_da_visita_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `benificiario_encaminhado`
--

DROP TABLE IF EXISTS `benificiario_encaminhado`;
CREATE TABLE IF NOT EXISTS `benificiario_encaminhado` (
  `id` int(11) NOT NULL,
  `benificiario_id` int(11) NOT NULL,
  `motivo_de_abertura_de_processo_id` int(11) NOT NULL,
  `documentos_necessarios_id` int(11) NOT NULL,
  `objectivo_da_visita_id` int(11) NOT NULL,
  `recebido_em` date DEFAULT NULL,
  `problema_resolvido` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_benificiario_encaminhado_benificiario1_idx` (`benificiario_id`),
  KEY `fk_benificiario_encaminhado_motivo_de_abertura_de_processo1_idx` (`motivo_de_abertura_de_processo_id`),
  KEY `fk_benificiario_encaminhado_documentos_necessarios1_idx` (`documentos_necessarios_id`),
  KEY `fk_benificiario_encaminhado_objectivo_da_visita1_idx` (`objectivo_da_visita_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contacto`
--

DROP TABLE IF EXISTS `contacto`;
CREATE TABLE IF NOT EXISTS `contacto` (
  `id` int(11) NOT NULL,
  `contacto` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `benificiario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contacto_benificiario1_idx` (`benificiario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `documentos_necessarios`
--

DROP TABLE IF EXISTS `documentos_necessarios`;
CREATE TABLE IF NOT EXISTS `documentos_necessarios` (
  `id` int(11) NOT NULL,
  `documentos` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `especificar_o_servico`
--

DROP TABLE IF EXISTS `especificar_o_servico`;
CREATE TABLE IF NOT EXISTS `especificar_o_servico` (
  `benificiario_encaminhado_id` int(11) NOT NULL,
  `servico_encaminhado_id` int(11) NOT NULL,
  `servico` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`benificiario_encaminhado_id`,`servico_encaminhado_id`),
  KEY `fk_benificiario_encaminhado_has_servico_encaminhado_servico_idx` (`servico_encaminhado_id`),
  KEY `fk_benificiario_encaminhado_has_servico_encaminhado_benific_idx` (`benificiario_encaminhado_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `motivo_de_abertura_de_processo`
--

DROP TABLE IF EXISTS `motivo_de_abertura_de_processo`;
CREATE TABLE IF NOT EXISTS `motivo_de_abertura_de_processo` (
  `id` int(11) NOT NULL,
  `motivo` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `objectivo_da_visita`
--

DROP TABLE IF EXISTS `objectivo_da_visita`;
CREATE TABLE IF NOT EXISTS `objectivo_da_visita` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `provincia`
--

DROP TABLE IF EXISTS `provincia`;
CREATE TABLE IF NOT EXISTS `provincia` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `servico_encaminhado`
--

DROP TABLE IF EXISTS `servico_encaminhado`;
CREATE TABLE IF NOT EXISTS `servico_encaminhado` (
  `id` int(11) NOT NULL,
  `servico` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sexo`
--

DROP TABLE IF EXISTS `sexo`;
CREATE TABLE IF NOT EXISTS `sexo` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `benificiario`
--
ALTER TABLE `benificiario`
  ADD CONSTRAINT `fk_benificiario_bairro1` FOREIGN KEY (`bairro_id`) REFERENCES `bairro` (`id`),
  ADD CONSTRAINT `fk_benificiario_objectivo_da_visita1` FOREIGN KEY (`objectivo_da_visita_id`) REFERENCES `objectivo_da_visita` (`id`),
  ADD CONSTRAINT `fk_benificiario_provincia` FOREIGN KEY (`provincia_id`) REFERENCES `provincia` (`id`),
  ADD CONSTRAINT `fk_benificiario_sexo1` FOREIGN KEY (`sexo_id`) REFERENCES `sexo` (`id`);

--
-- Constraints for table `benificiario_encaminhado`
--
ALTER TABLE `benificiario_encaminhado`
  ADD CONSTRAINT `fk_benificiario_encaminhado_benificiario1` FOREIGN KEY (`benificiario_id`) REFERENCES `benificiario` (`id`),
  ADD CONSTRAINT `fk_benificiario_encaminhado_documentos_necessarios1` FOREIGN KEY (`documentos_necessarios_id`) REFERENCES `documentos_necessarios` (`id`),
  ADD CONSTRAINT `fk_benificiario_encaminhado_motivo_de_abertura_de_processo1` FOREIGN KEY (`motivo_de_abertura_de_processo_id`) REFERENCES `motivo_de_abertura_de_processo` (`id`),
  ADD CONSTRAINT `fk_benificiario_encaminhado_objectivo_da_visita1` FOREIGN KEY (`objectivo_da_visita_id`) REFERENCES `objectivo_da_visita` (`id`);

--
-- Constraints for table `contacto`
--
ALTER TABLE `contacto`
  ADD CONSTRAINT `fk_contacto_benificiario1` FOREIGN KEY (`benificiario_id`) REFERENCES `benificiario` (`id`);

--
-- Constraints for table `especificar_o_servico`
--
ALTER TABLE `especificar_o_servico`
  ADD CONSTRAINT `fk_benificiario_encaminhado_has_servico_encaminhado_benificia1` FOREIGN KEY (`benificiario_encaminhado_id`) REFERENCES `benificiario_encaminhado` (`id`),
  ADD CONSTRAINT `fk_benificiario_encaminhado_has_servico_encaminhado_servico_e1` FOREIGN KEY (`servico_encaminhado_id`) REFERENCES `servico_encaminhado` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
